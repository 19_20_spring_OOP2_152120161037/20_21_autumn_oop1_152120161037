#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <Windows.h>
#include <iomanip>

using namespace std;

//Colors
inline std::ostream& blue(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE
		| FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	return s;
}
inline std::ostream& red(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,
		FOREGROUND_RED | FOREGROUND_INTENSITY);
	return s;
}
inline std::ostream& green(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,
		FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	return s;
}
inline std::ostream& yellow(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,
		FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
	return s;
}
inline std::ostream& magenta(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,
		FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	return s;
}
inline std::ostream& white(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,
		FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	return s;
}

//Reads the specified file: 
//Gets integers, its length and if there is an error, writes the error to errorMsg.
float* ReadIntegers(string fileName, int &length, string &errorMsg)
{
	float* sequence = 0; //Create a pointer for an array.
	ifstream stream;
	stream.open(fileName, ios::in); //Open the file.

	if (!stream.is_open()) //If the file does not exist, return an error message.
	{
		errorMsg = "The file does not exist!";
		return sequence;
	}

	string line; //Read the first line of the file.
	getline(stream, line);
	int count = 0; //How many is there number in the file?
	try
	{
		count = stoi(line); //Learn it.
		length = count; //Transfer the count of the numbers to referance length variable.

		if (count <= 0) //If count of the numbers is less than zero, return an error message.
		{
			errorMsg = "Input value is less than zero!";
			return sequence;
		}
	}
	catch (const std::exception&) //If data is not proper, return an error message.
	{
		errorMsg = "Your entered data is not proper!";
		return sequence;
	}

	sequence = new float[count]; //Then, create an array into the determined address.

	getline(stream, line); //Read the second line.

	string str = "";
	int counter = 0; 
	for (auto x : line) //Hands each element of the second line as char type.
	{
		if (x == ' ')
		{
			sequence[counter] = stof(str); //Transfer each number to the array.
			counter++; //Count number of the numbers in data.
			str = "";
		}
		else
			str = str + x;
	}

	if ((counter + 1) != count) //Return an error message unless the count of the specified 
								//data in first line and the count of data that already exist match.
	{
		errorMsg = "There is not enough data for the stated number!";
		sequence = 0;
		return sequence;
	}
	sequence[counter] = stof(str);

	return sequence;
}

//Add all elements of the array to each other.
float Sum(float* arr, int length)
{
	float sum = 0;
	for (int i = 0; i < length; i++)
		sum += arr[i];
	
	return sum;
}
//Multiply all elements of the array.
float Product(float* arr, int length)
{
	float product = 1;
	for (int i = 0; i < length; i++)
		product *= arr[i];

	return product;
}
//Computes the average of the array.
float Average(float* arr, int length)
{
	return (Sum(arr, length) / length);
}
//Finds the smallest element of the array.
float Smallest(float* arr, int length)
{
	float min = arr[0];

	for (int i = 0; i < length; i++)
	{
		if (min > arr[i])
			min = arr[i];
	}

	return min;
}

void main()
{
	int length;
	string errorMsg = "";
	//Read the file and get all data: array, length and if any error message.
	float* arr = ReadIntegers("input.txt", length, errorMsg);

	if (errorMsg != "") //There is a error.
	{
		cout << magenta << errorMsg << white << endl;
	}
	else
	{
		cout << blue << "  +" << white << "---------" << blue << "+" << white << "---------"
			<< blue << "+" << endl << white << "  | " << magenta << "  SUM   " << white << "|"
			<< green << setw(9) << setfill(' ') << right << Sum(arr, length) << white << "|"
			<< endl << blue << "  +" << white << "---------" << blue << "+" << white << "---------"
			<< blue << "+" << endl << white << "  |" << magenta << " PRODUCT " << white << "|"
			<< green << setw(9) << setfill(' ') << right << Product(arr, length) << white << "|"
			<< endl << blue << "  +" << white << "---------" << blue << "+" << white << "---------"
			<< blue << "+" << endl << white << "  |" << magenta << " AVERAGE " << white << "|"
			<< green << setw(9) << setfill(' ') << right << Average(arr, length) << white << "|"
			<< endl << blue << "  +" << white << "---------" << blue << "+" << white << "---------"
			<< blue << "+" << endl << white << "  |" << magenta << "SMALLEST " << white << "|"
			<< green << setw(9) << setfill(' ') << right << Smallest(arr, length) << white << "|"
			<< endl << blue << "  +" << white << "---------" << blue << "+" << white << "---------"
			<< blue << "+" << white << endl;
	}

	cout << endl;
	system("pause");
}