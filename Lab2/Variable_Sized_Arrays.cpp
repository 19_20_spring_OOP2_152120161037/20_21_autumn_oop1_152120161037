#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

typedef struct Point
{
    int X;
    int Y;
}Point;

int main() 
{
    int n; //Number of arrays
    int q; //Number of quary

    cin >> n >> q;

    int** matrix = new int*[n]; //Create a matrix has (n) row.

    int m;
    for (int i = 0; i < n; i++)
    {
        cin >> m;
        matrix[i] = new int[m];
        for (int j = 0; j < m; j++)
        {
            cin >> matrix[i][j];
        }
    }

    Point* points = new Point[q];
    for (int i = 0; i < q; i++)
    {
        Point p;
        cin >> p.X >> p.Y;
        points[i] = p;
    }

    for (int i = 0; i < q; i++)
    {
        cout << matrix[points[i].X][points[i].Y] << endl;
    }
    
    return 0;
}