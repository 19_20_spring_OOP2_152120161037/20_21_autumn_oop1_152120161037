#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str)
 {
  stringstream ss(str);
int x;
vector<int> vec;

while (ss >> x){
    vec.push_back(x);

    if (ss.peek() == ','){
        ss.ignore();
    }
}

return vec;   
 }

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }
    
    return 0;
}